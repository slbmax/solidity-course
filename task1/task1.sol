pragma solidity ^0.8.9;

interface IDataTypesPractice {
    function getInt256() external view returns(int256);
    function getUint256() external view returns(uint256);
    function getIint8() external view returns(int8);
    function getUint8() external view returns(uint8);
    function getBool() external view returns(bool);
    function getAddress() external view returns(address);
    function getBytes32() external view returns(bytes32);
    function getArrayUint5() external view returns(uint256[5] memory);
    function getArrayUint() external view returns(uint256[] memory);
    function getString() external view returns(string memory);

    function getBigUint() external pure returns(uint256);
}

contract Test is IDataTypesPractice{
    int256 myInt256 = -123322333232;
    uint256 myUint256 = 123456789;
    int8 myInt8 = -5;
    uint8 myUint8 = 5;
    bool myBool = true;
    address myAddres = address(this);
    bytes32 myBytes32 = "Hi!";
    uint256[5] myUintArr5 = [1,2,3,4,5];
    uint256[] myUintArr = [uint256(1),2];
    string myString = "Hello World!";

    function getInt256() external view returns(int256){
        return myInt256;
    }

    function getUint256() external view returns(uint256){
        return myUint256;
    }

    function getIint8() external view returns(int8){
        return myInt8;
    }

    function getUint8() external view returns(uint8){
        return myUint8;
    }

    function getBool() external view returns(bool){
        return myBool;
    }

    function getAddress() external view returns(address){
        return myAddres;
    }

    function getBytes32() external view returns(bytes32){
        return myBytes32;
    }

    function getArrayUint5() external view returns(uint256[5] memory){
        return myUintArr5;
    }

    function getArrayUint() external view returns(uint256[] memory){
        return myUintArr;
    }

    function getString() external view returns(string memory){
        return myString;
    }

    function getBigUint() external pure returns(uint256){
        uint256 v1 = 1;
        uint256 v2 = 2;
        return v1<<(v2**(v1+v2*v2));
    }
}